// COPYRIGHT :)

struct PSInput
{
	float4 position : SV_POSITION;
	float4 color    : COLOR;
};

cbuffer FrameParams : register(b1)
{
    float4x4 viewProjectionMatrix;
    float4   eyePos;
};

PSInput vs_main(float4 position : POSITION, float4 color : COLOR)
{
	PSInput result;
    result.position = mul(position, viewProjectionMatrix);
	result.color = color;
	return result;
}

struct PS_OUT
{
    float4 diffuse : SV_TARGET0;
    float4 normals : SV_TARGET1;
    float  depth   : SV_TARGET2;
};

PS_OUT ps_main(PSInput input)
{
	PS_OUT output;
	 
	output.depth = input.position.z;
	output.diffuse = float4(1.0, 1.0, 1.0, 1.0);
	output.normals = float4(0.0, 0.0, -1.0, 1.0);
	return output;
}

[maxvertexcount(8)]
void gs_main(point PSInput input[1], inout  LineStream<PSInput> OutputStream)
{
	PSInput output = input[0];

	output.position.x = input[0].position.x - 0.01;
	output.position.y = input[0].position.y;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	output.position.x = input[0].position.x + 0.01;
	output.position.y = input[0].position.y;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	OutputStream.RestartStrip();

	output.position.x = input[0].position.x - 0.01 * 0.7;
	output.position.y = input[0].position.y + 0.01 * 1.77f  * 0.7;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	output.position.x = input[0].position.x + 0.01  * 0.7;
	output.position.y = input[0].position.y - 0.01* 1.77f  * 0.7;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	OutputStream.RestartStrip();

	output.position.x = input[0].position.x;
	output.position.y = input[0].position.y - 0.01* 1.77f;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	output.position.x = input[0].position.x;
	output.position.y = input[0].position.y + 0.01* 1.77f;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	OutputStream.RestartStrip();

	output.position.x = input[0].position.x + 0.01  * 0.7;
	output.position.y = input[0].position.y + 0.01* 1.77f  * 0.7;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	output.position.x = input[0].position.x - 0.01  * 0.7;
	output.position.y = input[0].position.y - 0.01* 1.77f  * 0.7;
	output.position.z = input[0].position.z;
	output.color = input[0].color;
	OutputStream.Append(output);

	OutputStream.RestartStrip();
}

cbuffer cbCS : register(b0)
{
	uint	g_param;	// param[0] = MAX_PARTICLES;
};

float rand(float2 co){
	return (frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453));
}

RWStructuredBuffer<PSInput> uavVertexes	: register(u0);	// UAV

#define THREAD_GROUP_X  32

[numthreads(THREAD_GROUP_X, 1, 1)]
void cs_main(uint3 ThreadID : SV_GroupThreadID, uint3 GroupID : SV_GroupID)
{
	uint id = GroupID.x * THREAD_GROUP_X + ThreadID.x;

	PSInput vertex = uavVertexes[id];
	float randomX = rand(float2(vertex.position.y, vertex.position.x)) * 0.0005;
	float randomY = rand(float2(vertex.position.x, vertex.position.y)) * 0.0005;
			
	uavVertexes[id].position.y -= 0.001;// +randomY;

	if (uavVertexes[id].position.y < -1.0)
	{
		uavVertexes[id].position.y *= -1.0;
	}

	uavVertexes[id].position.x -= randomX;

	if (uavVertexes[id].position.x < -1.0)
	{
		uavVertexes[id].position.x += 2.0;
	}

	if (uavVertexes[id].position.x > 1.0)
	{
		uavVertexes[id].position.x -= 2.0;
	}
}
